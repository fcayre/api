# coding: utf-8

import sys
import csv
import os.path as osp
from configparser import RawConfigParser
from time import time
from datetime import datetime
from collections import defaultdict

import requests
from lxml.etree import FunctionNamespace
from lxml.html import fromstring as html_parse

BASE_URL = 'https://www.balotilo.org'


CANDIDATE_FILE = 'candidats.csv'
VOTERS_FILE = 'votants.csv'
RESULTS_FILE = 'results.csv'
TITLE_TEMPLATE = 'API - Élection parents délégués - 2022/2023 - %s'
CANDIDATE_TEMPLATE = '%(Prénom)s %(Nom)s - Parent de : %(Élève)s'

H_CLASS = 'Classe'
H_SCHOOL = 'École'
H_PARTICIP = 'Participation'
H_MAIL_ERRORS = 'Erreur(s) mail'
H_VOTES = 'Nombre de voix'


# Create and register the xpath "ends-with" function
FunctionNamespace(None)['ends-with'] = lambda c, s, arg: s.endswith(arg) and s


def get_conf(name, path=None):
    path = osp.expanduser(path or osp.join('~', '.config', 'balotilo.ini'))
    parser = RawConfigParser()
    parser.read(path)
    return dict(parser.items(name))


def get_auth_token(resp, form_id):
    return html_parse(resp.text).xpath(
        '//form[@id="%s"]'
        '//input[@name="authenticity_token"]/@value' % form_id)[0]


def assert_title(resp, title, tag='h2'):
    assert title in html_parse(resp.text).xpath('//%s/text()' % tag)


def start_session(conf_name, conf_path=None):
    conf = get_conf(conf_name, conf_path)
    session = requests.Session()
    resp1 = session.get(BASE_URL + '/login')
    resp1.raise_for_status()
    token1 = get_auth_token(resp1, 'new_user_session')

    resp2 = session.post(BASE_URL + '/user_session', data={
        'utf8': '\u2713',
        'authenticity_token': token1,
        'user_session[email]': conf['login'],
        'user_session[password]': conf['password'],
        'user_session[remember_me]': '0',
        'commit': 'Connexion',
    })
    resp2.raise_for_status()
    assert_title(resp2, 'Élections')
    return session


def find_matching_line(lines, text, post_func=None):
    for line in lines:
        if text.lower() in line.lower():
            if post_func:
                return post_func(line)
            return line


def parse_participation(s):
    return float(s.split(':', 1)[1].replace('%', '').strip()) / 100.


def parse_vote_summary(container):
    all_text = container.text_content().splitlines()
    title = container.xpath('.//div[@class="title"]/b/text()')[0]
    if not title.startswith(TITLE_TEMPLATE[:20]):
        return
    return {
        'class': title.rsplit(' - ', 1)[1],
        'state': container.xpath('.//div[@class="title"]/span/text()')[0],
        'participation': find_matching_line(
            all_text, 'participation', parse_participation),
        'errors': find_matching_line(
            all_text, 'courriel',
            lambda l: int(l.strip().split(' ', 1)[0].replace('Aucun', '0')))
    }


def parse_vote_summaries(session, func=parse_vote_summary):
    page = 0
    while True:
        page += 1
        resp = session.get(BASE_URL + '/?page=%d' % page)
        resp.raise_for_status()
        # iprint("Got page %d" % page)
        doc = html_parse(resp.text)
        containers = doc.xpath('//div[@class="consultation-content"]')
        if containers:
            for container in containers:
                result = func(container)
                if result:
                    yield result
        else:
            break


def print_vote_summary_csv(session, votes=None, out=sys.stdout):
    if votes is None:
        votes = list(parse_vote_summaries(session))
    out.write(','.join([H_CLASS, H_PARTICIP, H_MAIL_ERRORS])+'\n')
    for vote in sorted(votes, key=lambda v: v['class']):
        line = '%(class)s,%(participation)s,%(errors)s\n' % vote
        out.write(line)


def dump_vote_summary_csv(conf='moi'):
    """ To be used in a crontab like::
      0 * * * * cd <HERE> && python3 -c "import balotilo ; balotilo.dump_vote_summary_csv('api')"
    """
    session = start_session(conf)
    fname = conf + datetime.now().strftime('_resultats_%Y%m%d-%Hh%Mmin%Ss.csv')
    with open(fname, 'w') as fobj:
        print_vote_summary_csv(session, out=fobj)


def parse_vote_result(doc):
    all_text = doc.xpath(
        '//div[@class="breath"]')[0].text_content().splitlines()
    result = {
        'title': doc.xpath(
            '//div[@class="panel-heading"]/div/text()')[0].strip(),
        'participation': find_matching_line(
            all_text, 'participation', parse_participation),
        'votes': find_matching_line(
            all_text, 'exprimé',
            lambda s: int(s.strip().split(' ')[0].replace('(', ''))),
        'candidates': {},
        }
    vote_rows = doc.xpath('//div[@class="question"]//table//tr')
    for td1, td2 in vote_rows:
        candidate = td1.text_content()
        votes = int(td2.text_content())
        result['candidates'][candidate] = votes
    return result


def parse_vote_results(session):
    results = {}
    page = 0
    while True:
        page += 1
        resp = session.get(BASE_URL + '/?page=%d' % page)
        resp.raise_for_status()
        html_page = html_parse(resp.text)
        titles = html_page.xpath('//div[@class="title"]/b/text()')
        result_paths = html_page.xpath(
                '//a[ends-with(string(@href), "/results")]/@href')
        if not result_paths:
            break
        for title, path in zip(titles, result_paths):
            if not title.startswith(TITLE_TEMPLATE % ''):
                print(u"Ignoring %s vote" % title)
                continue
            resp = session.get(BASE_URL + path)
            resp.raise_for_status()
            doc = html_parse(resp.text)
            result = parse_vote_result(doc)
            results[result['title']] = result
    return results


def print_vote_results(session, results=None):
    if results is None:
        results = parse_vote_results(session)

    with open(CANDIDATE_FILE) as fobj_read:
        reader = csv.DictReader(fobj_read, delimiter=',')
        with open(RESULTS_FILE, 'w') as fobj_write:
            fieldnames = reader.fieldnames + [H_VOTES, H_PARTICIP]
            writer = csv.DictWriter(fobj_write, delimiter=',',
                                    fieldnames=fieldnames)
            writer.writeheader()
            for row in reader:
                title = TITLE_TEMPLATE % row[H_CLASS]
                candidate = (CANDIDATE_TEMPLATE % row).strip()
                if title not in results:
                    print('WARNING: %s not found in results!' % title)
                    continue
                row[H_VOTES] = results[title]['candidates'][candidate]
                row[H_PARTICIP] = results[title]['participation']
                writer.writerow(row)


def _action_on_matching_votes(session, query, perform_action_func, debug=False):
    result = []
    resp = session.get(BASE_URL)
    doc = html_parse(resp.text)
    page_nb = len(doc.xpath("//div[@class='pagination'][1]//a"))
    print('Found %d Pages!' % page_nb)

    for page_num in range(page_nb, 0, -1):
        resp = session.get(BASE_URL + '/?page=%d' % page_num)
        resp.raise_for_status()
        if debug:
            print("Got page %d/%d" % (page_num, page_nb))
        doc = html_parse(resp.text)
        token = doc.xpath('//meta[@name="csrf-token"]/@content')[0]
        for container in doc.xpath('//div[@class="consultation-content"]'):
            title = container.xpath('.//div[@class="title"]/b/text()')[0]
            if debug:
                print("Detected vote: %s" % title)
            if query in title:
                vote_num = container.xpath(
                    './/a[text()="Modifier"]/@href'
                )[0].split('/')[2]
                print('Detected vote num %s: %s' % (vote_num, title))
                if not debug:
                    perform_action_func(session, token, vote_num)
                    result.append(vote_num)
            elif debug:
                print('> Does not match query')

    return result


def _delete_vote(session, token, vote_num):
    resp = session.post(BASE_URL + '/consultations/%s' % vote_num,
                        {'_method': 'delete', 'authenticity_token': token})
    resp.raise_for_status()
    print('> Deleted!')


def _start_vote(session, token, vote_num):
    resp = session.get(BASE_URL + '/consultations/%s/publish' % vote_num)
    resp.raise_for_status()
    print('> Started!')


def delete_matching_votes(session, query, debug=False):
    return _action_on_matching_votes(session, query, _delete_vote, debug=debug)


def start_matching_votes(session, query, debug=False):
    return _action_on_matching_votes(session, query, _start_vote, debug=debug)


def new_vote(session, title, association, description,
             date_end, date_end_picker,
             vote_descr, candidates, choice_limit, voters):

    resp3 = session.get(BASE_URL + '/consultations/new')
    resp3.raise_for_status()
    assert_title(resp3, 'Nouvelle élection')
    token3 = get_auth_token(resp3, 'new_consultation')

    qattrs = 'consultation[questions_attributes][%s]' % int(time() * 1000)

    limit = min(choice_limit, len(candidates))
    data = [
        ('utf8', '\u2713'),
        ('authenticity_token', token3),
        ('consultation[title]', title),
        ('consultation[community]', association),
        ('consultation[description]', description),
        ('_wysihtml5_mode', '1'),
        ('consultation[voting_method]', 'secret_ballot'),
        ('consultation[ending]', date_end),
        ('ending_picker', date_end_picker),
        ('consultation[locale]', 'fr'),
        ('%s[_destroy]' % qattrs, 'false'),
        ('%s[content]' % qattrs, vote_descr),
        ('_wysihtml5_mode', '1'),
        ('%s[type_helper]' % qattrs, 'Approval'),
        ('%s[approval_new_candidates]' % qattrs, '<br>'.join(candidates)),
        ('_wysihtml5_mode', '1'),
        ('%s[approval_new_limit]' % qattrs, str(limit)),
        ('commit', 'Valider'),
    ]
    resp4 = session.post(BASE_URL + '/consultations', data=data)
    resp4.raise_for_status()

    assert len(resp4.history) == 1 and resp4.history[0].status_code == 302, (
        'expected redirect on vote creation')
    import_voters_url = resp4.history[0].headers['Location']
    vote_id = int(import_voters_url.rsplit('/', 2)[1])
    action = "/consultations/%d/simulate_import_new_voters" % vote_id
    token4 = html_parse(resp4.text).xpath(
        '//form[@action="' + action + '"]//input[@name="authenticity_token"]/@value')[0]

    resp5 = session.post(
        BASE_URL + action,
        data={
            'authenticity_token': token4,
            '_method': 'patch',
            'consultation[new_voters_emails]': ','.join(voters),
            'commit': 'Continuer',
        })
    resp5.raise_for_status()

    action = "/consultations/%d/import_new_voters" % vote_id
    token5 = html_parse(resp5.text).xpath(
        '//form[@action="' + action + '"]//input[@name="authenticity_token"]/@value')[0]

    resp6 = session.post(
        BASE_URL + action,
        data={
            'authenticity_token': token5,
            '_method': 'patch',
            'consultation[new_voters_emails]': ','.join(voters),
            'commit': 'Importer',
        })
    resp6.raise_for_status()


def main(conf_name='moi'):

    session = start_session(conf_name)
    conf = get_conf(conf_name)

    CHOICE_LIMITS = {'Maternelle': 1, 'Primaire': 1, 'Collège': 2, 'Lycée': 2}

    data = {
        'date_end': '2022-10-09 23:30:00  02:00',
        'date_end_picker': '09/10/2022 23:30',
        'association': conf['association'],
        'description': conf['description'],
        'vote_descr': (
            'Cochez la ou les personnes que vous souhaitez élire comme '
            'parent(s) délégué(s) de classe parmi les candidats suivants :'),
    }

    def init_class_data():
        result = data.copy()
        result.update({
            'candidates': [],
            'voters': set(),
        })
        return result

    vote_data = defaultdict(init_class_data)

    with open(CANDIDATE_FILE) as fobj:
        reader = csv.DictReader(fobj, delimiter=',')
        for row in reader:
            classroom_data = vote_data[row[H_CLASS]]
            classroom_data['title'] = TITLE_TEMPLATE % row[H_CLASS]
            classroom_data['choice_limit'] = CHOICE_LIMITS[row[H_SCHOOL]]
            classroom_data['candidates'].append(CANDIDATE_TEMPLATE % row)

    with open(VOTERS_FILE) as fobj:
        reader = csv.DictReader(fobj, delimiter=',')
        for row in reader:
            if not row[H_CLASS]:
                continue  # "Hors Contrat" implies this case
            classroom_data = vote_data[row[H_CLASS]]
            classroom_data['voters'] |= {
                row['Email%d' % i] for i in (1, 2) if row['Email%d' % i]}

    for classroom, data in sorted(vote_data.items()):
        if not data['candidates']:
            print('No candidates for classroom %r: skipping...' % classroom)
            continue
        new_vote(session, **data)
        print('VOTE: %s (%s candidates, %s voters)' % (
            data['title'], len(data['candidates']), len(data['voters'])))

    return session, vote_data
