# coding: utf-8

import csv
import logging
import re
import sys
import os.path as osp
from configparser import RawConfigParser
from datetime import date, timedelta

import requests
from lxml.html import fromstring as html_parse
from lxml.etree import FunctionNamespace


logging.basicConfig(stream=sys.stdout, level=logging.INFO)


# Create and register the xpath "ends-with" function
FunctionNamespace(None)['ends-with'] = lambda c, s, arg: s.endswith(arg) and s
FunctionNamespace(None)['hasclass'] = lambda c, *args: set(c.context_node.attrib.get('class', '').split()).issuperset(args)


def purge_file_from_form(data):
    for k, v in list(data.items()):
        if '_image' in k or '_file' in k:
            del data[k]


class Helper(object):

    ini_filename = None
    base_url = None
    login_page_path = None
    login_form_id = None
    login_field_name = None
    password_field_name = None

    def __init__(self, conf_name, session=None, **kwargs):
        self.conf = self._get_conf(conf_name)
        for key, value in kwargs.items():
            if hasattr(self, key):
                setattr(self, key, value)
        self.session = session

    def _get_conf(self, name, path=None):
        path = osp.expanduser(
            path or osp.join('~', '.config', self.ini_filename))
        parser = RawConfigParser()
        parser.read(path)
        return dict(parser.items(name))

    # Low-level helpers

    def get(self, url_or_path, **kwargs):
        if url_or_path.startswith('/'):
            url_or_path = self.base_url + url_or_path
        return self.session.get(url_or_path, **kwargs)

    def post(self, url_or_path, **kwargs):
        if url_or_path.startswith('/'):
            url_or_path = self.base_url + url_or_path
        return self.session.post(url_or_path, **kwargs)

    @classmethod
    def assert_title(cls, resp, title):
        tree = html_parse(resp.text)
        assert title == tree.xpath('.//h1[@id="page-title"]/text()')[0].strip()
        return tree

    @classmethod
    def assert_tab_active(cls, resp, title):
        tree = html_parse(resp.text)
        assert title == tree.xpath('.//ul[hasclass("tabs")]//li[hasclass("active")]/a/text()')[0]
        return tree

    # High-level helpers

    def get_and_submit_form(self, url, form_id, transform_data_func=None,
                            allow_redirects=True, **form_update_fields):
        resp1 = self.get(url)
        url, data = self.simple_form_data(resp1, form_id)
        data.update(form_update_fields)
        if transform_data_func:
            transform_data_func(data)
        logging.debug('get_and_submit_form: posting data=%s', data)
        resp2 = self.post(url, data=data, allow_redirects=allow_redirects)
        resp2.raise_for_status()
        return resp2

    def simple_form_data(self, resp, form_id):
        data = {}
        form = html_parse(resp.text).xpath('.//form[@id="%s"]' % form_id)[0]
        for input in form.xpath('.//input[@name][@value]'):
            name = input.get('name')
            if name not in data:
                data[name] = input.get('value')
            else:
                if not isinstance(data[name], list):
                    data[name] = [data[name]]
                data[name].append(input.get('value'))
        for select in form.xpath('.//select[@name]'):
            for opt in select.xpath('.//option[@selected]'):
                data[select.get('name')] = opt.get('value')
        return form.get('action'), data

    def start_session(self):
        self.session = requests.Session()
        login_data = {
            self.login_field_name: self.conf['login'],
            self.password_field_name: self.conf['password'],
        }
        resp = self.get_and_submit_form(
            self.login_page_path, self.login_form_id, **login_data)
        self.assert_title(resp, self.conf['login'])


class Framaforms(Helper):

    ini_filename = 'framaforms.ini'
    base_url = 'https://framaforms.org'
    login_page_path = '/user'
    login_form_id = 'user-login'
    login_field_name = 'name'
    password_field_name = 'pass'

    def list_forms(self, query=None):
        forms = {}
        user = self.conf['login'].lower().replace(' ', '-')
        params = {'page': 0}
        if query:
            params['title'] = query
        while True:
            logging.debug('Page %s', params['page'])
            resp = self.get('/users/%s/forms' % user, params=params)
            tree = self.assert_title(resp, 'Mes formulaires')
            links = tree.xpath('.//table//tr/td[position() = 1]/a')
            logging.debug('Found %d forms: %s', len(links),
                          ', '.join(l.get('href') for l in links))
            if not links:
                break
            for l in links:
                forms[l.text] = l.get('href')
            params['page'] += 1
        return forms

    def delete_forms(self, forms, interactive=True):
        "Delete given forms in the format of a `list_forms` method call"
        if interactive:
            input('Type CTRL-C to stop')
        for num, (form_name, form_url) in enumerate(forms.items(), 1):
            logging.info('Deleting form %d/%d: %s...'
                         % (num, len(forms), form_name))
            self.delete_form(form_url)

    def delete_form(self, form_url):
        delete_url = form_url + '/delete'
        resp1 = self.get(delete_url)
        resp1.raise_for_status()
        url, data = self.simple_form_data(resp1, 'node-delete-confirm')
        resp2 = self.post(url, data=data)
        resp2.raise_for_status()

    def clone_form(self, form_url, **kw):
        resp1 = self.get(form_url + '/share')
        resp1.raise_for_status()
        tree = self.assert_tab_active(resp1, 'Partager')

        kw['op'] = 'Enregistrer'
        kw.setdefault('transform_data_func', purge_file_from_form)
        kw['allow_redirects'] = False

        clone_url = tree.xpath(
            './/a[ends-with(string(@href), "/clone/confirm")]/@href')[0]
        return self.get_and_submit_form(clone_url, 'form1-node-form', **kw)

    def change_signature(self, builder_url, signature):
        "Hypothesis is that the signature is in last 'form-builder-element'"
        resp1 = self.get(builder_url)
        ctx = {
            'node_id': re.match('.*/node/(?P<node_id>[0-9]+)/.*',
                                builder_url).group('node_id'),
            'field_id': html_parse(resp1.text).xpath(
                './/div[contains(@class, "form-builder-element")'
                ' and contains(., "SIGNATURE")]/@id')[0].rsplit('-', 1)[-1],
            }

        config_field_url = (
            '/admin/structure/form-builder/configure/webform'
            '/%(node_id)s/%(field_id)s' % ctx)
        self.get_and_submit_form(
            config_field_url, 'form-builder-field-configure',
            **{'markup[value]': signature})

        url, data = self.simple_form_data(
            resp1, 'form-builder-webform-save-form')
        data['op'] = 'Enregistrer'
        return self.post(url, data=data)


def transform_form_data(data, limit_date, **kwargs):
    purge_file_from_form(data)
    if 'intro' in kwargs:
        data['body[und][0][value]'] = kwargs['intro']


def class_council_form(helper, orig_form_url, limit_date, intro, signature,
                       **data):
    expiration = (date.today() + timedelta(days=30*6)).strftime('%Y-%m-%d')
    data.update({
        'field_form1_expiration[und][0][value][date]': expiration,
        'transform_data_func': lambda d: transform_form_data(d, limit_date,
                                                             intro=intro),
    })
    resp = helper.clone_form(orig_form_url, **data)
    resp2 = helper.change_signature(resp.headers['Location'], signature)
    tabs = html_parse(resp2.text).xpath('.//div[@class="tabs"]/ul[1]')[0]
    return {
        'Lien pour les parents': (
            helper.base_url + tabs.xpath('.//li[1]//a/@href')[0]),
        'Lien de récupération des résultats': (
            helper.base_url + tabs.xpath('.//li[4]//a/@href')[0]),
        }


def start_helper(conf_name='api'):
    helper = Framaforms(conf_name)
    helper.start_session()
    return helper


def main(conf_name='api', session=None):

    HERE = osp.abspath(osp.dirname(__file__))

    data = []
    fpath = osp.join(HERE, 'conseils_de_classe.csv')
    with open(fpath) as fobj:
        reader = csv.DictReader(fobj, delimiter=',')
        for line in reader:
            data.append(line)

    helper = Framaforms(conf_name, session)
    if session is None:
        helper.start_session()

    template_forms = helper.list_forms('MODÈLE - CC')

    with open('conseils_de_classe_signature.txt') as fobj:
        form_sign = fobj.read().strip()

    with open('conseils_de_classe_intro.html') as fobj:
        intro = fobj.read().strip()

    with open('conseils_de_classe_titre.txt') as fobj:
        title = fobj.read().strip()

    for num, item in enumerate(data):
        logging.info('CC %d/%d', num+1, len(data))
        template_url = template_forms[item['Modèle']]
        retry = True
        while retry:
            try:
                retry = False
                form_data = class_council_form(
                    helper, template_url, item['Date limite'],
                    intro % item, form_sign % item, title=title % item)
            except KeyboardInterrupt:
                retry = True
        print(form_data)
        item.update(form_data)

    fieldnames = reader.fieldnames
    if 'Lien pour les parents' not in fieldnames:
        fieldnames.extend([
            'Lien pour les parents',
            'Lien de récupération des résultats',
        ])
    with open(fpath, 'w') as fobj:
        writer = csv.DictWriter(fobj, delimiter=',', fieldnames=fieldnames)
        writer.writeheader()
        writer.writerows(data)

    return data


if __name__ == '__main__':
    main()
